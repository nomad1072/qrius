var express = require('express');
var bodyParser = require('body-parser');


var userController = require('./controllers/user.js')
var messageController = require('./controllers/message.js');
var { authenticate } = require('./middleware/authenticate.js');
var app = express();

app.use(bodyParser.json())

app.post('/register', userController.addUser);
app.post('/login', userController.login);
app.post('/message', authenticate, messageController.postMessage);
app.get('/message', authenticate, messageController.getAllMessages);
app.get('/message/:id', authenticate, messageController.getMessage);

app.listen(3000, () => {
  console.log('Server started on port 3000');
})