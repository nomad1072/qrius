var { Message } = require('../models/message.js');
var config = require('../config/config');
process.env.GOOGLE_APPLICATION_CREDENTIALS = config.google_application_credentials;

const language = require('@google-cloud/language');

// Instantiates a client
const client = new language.LanguageServiceClient();

exports.postMessage = (req, res) => {
  const text = req.body.text;
  const document = {
    content: text,
    type: 'PLAIN_TEXT',
  };
  var message = new Message({
    text: req.body.text,
    creator: req.user._id
  });
  client
    .analyzeSentiment({ document: document })
    .then(results => {
      const sentiment = results[0].documentSentiment;
      message.score = sentiment.score;
      message.magnitude = sentiment.magnitude;
      return message.save()
    })
    .then((doc) => {
      res.status(201).send(doc);
    })
    .catch((err) => {
      res.status(404).send(err);
    })
};

exports.getAllMessages = (req, res) => {
  Message.find({ creator: req.user._id })
    .then((docs) => {
      res.status(201).send(docs)
    })
    .catch((err) => {
      console.log('No Messages to SHow!');
      res.status(404).send(err);
    })
};

exports.getMessage = (req, res) => {
  // if (!ObjectID.isValid(id)) {
  //   console.log('ID not valid');
  //   res.status(404).send();
  // }
  console.log('Id passed as query params: ', req.params.id);
  Message.find({ _id: req.params.id, creator: req.user._id }).sort("timeStamp: -1")
    .then((docs) => {
      const message = docs[0];
      res.status(200).send(docs[0]);
    })
    .catch((err) => {
      console.log("in catch of gte message");
      res.status(404).send("No such message found")
    })
};

