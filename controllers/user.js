var { User } = require('../models/user.js');

exports.addUser = (req, res) => {
  var user = new User({});

  user.email = req.body.email;
  user.password = req.body.password;
  user.username = req.body.username;
  user.firstName = req.body.name.split(" ")[0];
  user.lastName = req.body.name.split(" ")[1] || "";

  user.save()
    .then(() => {
      return user.generateAuthToken();
    })
    .then((token) => {
      res.header('jwt-auth', token).send(user);
    })
    .catch((e) => {
      console.log('Spmething is wrong: ', e);
      res.status(400).send(e);
    })
};

exports.login = (req, res) => {
  var email = req.body.email;
  var password = req.body.password;
  User.findByCredentials(email, password)
    .then((user) => {
      user.generateAuthToken()
        .then((token) => {
          res.header('jwt-auth', token).send(user);
        });
    })
    .catch((e) => {
      res.status(400).send();
    });
};