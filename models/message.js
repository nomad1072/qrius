var { mongoose } = require('../db/mongoose');

var MessageSchema = new mongoose.Schema({
  text: { type: String, required: true },
  timeStamp: { type: Date, default: Date.now },
  creator: {
    type: mongoose.Schema.Types.ObjectId,
    required: true
  },
  score: { type: Number, required: true },
  magnitude: { type: Number, required: true }
});

var Message = mongoose.model('Message', MessageSchema);
module.exports = {
  Message
}